﻿#include <iostream>
using namespace std; // чтобы не указывать префикс std;
void PrintNumbers(int Limit, bool IsEven)
{
	for (int i = IsEven ? 0 : 1; i <= Limit; i += 2)//тернарный оператор с булевой, тру1 четные, фолс0 - нечетные
	{
		cout << i << " ";
	}
	cout << endl;
}

int main()
{
	int Limit;
	cout << "Enter the limit: \n";
	cin >> Limit;
	bool IsEven;
	cout << "Odd (0) or even (1) numbers \n";
	cin >> IsEven;

	if (IsEven == 1)
	{
	cout << "Even numbers:";
	PrintNumbers(Limit, true);
	}

	if (IsEven == 0)
	{
		cout << "Odd numbers: \n";
	PrintNumbers(Limit, false);
	}

	return 0;
}
